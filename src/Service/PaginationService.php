<?php

namespace App\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Twig\Environment;

class PaginationService{
  private $entityClass;
  private $limit=10;
  private $currentPage=1;
  private $manager;
  private $twig;
  private $route;
  private $templatePath;

  public function __construct(ObjectManager $manager,Environment $twig, $templatePath){
    $this->manager=$manager;
      $this->twig=$twig;
      $this->templatePath=$templatePath;
  }

  public function display(){
    $this->twig->display($this->templatePath,[
      'page'=>$this->currentPage,
      'pages'=>$this->getPages(),
      'route'=>$this->route
    ]);
  }

  public function getPages(){
    $repo=$this->manager->getRepository($this->entityClass);
    $total=count($repo->findAll());
    $pages=ceil($total/$this->limit);
    return $pages;
  }

public function getData(){
  $offset=$this->currentPage * $this->limit - $this->limit;
  $repo=$this->manager->getRepository($this->entityClass);
  $data=$repo->findBy([],[],$this->limit,$offset);
  return $data;
}

public function setTemplatePath($templatePath){
  $this->templatePath=$templatePath;
  return $this;
}

public function getTemplatePath(){
  return $this->templatePath;
}

  public function setPage($page){
    $this->currentPage=$page;
    return $this;
  }

  public function getPage(){
    return $this->currentPage;
  }

  public function setRoute($route){
    $this->route=$route;
    return $this;
  }

  public function getRoute(){
    return $this->route;
  }

  public function setLimit($limit){
    $this->limit=$limit;
    return $this;
  }

  public function getLimit(){
    return $this->limit;
  }
  public function setEntityClass($ec){
    $this->entityClass=$ec;
    return $this;
  }
  public function getEntityClass(){
    return $this->entityClass;
  }
}
