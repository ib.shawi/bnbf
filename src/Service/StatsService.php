<?php

namespace App\Service;

use Doctrine\Common\Persistence\ObjectManager;

class StatsService{
  private $manager;

  public function __construct(ObjectManager $manager){
    $this->manager=$manager;
  }

public function getUsersCount(){
  return $this->manager->createQuery('select count(u) from App\Entity\User u')->getSingleScalarResult();

}
public function getAdsCount(){
return $this->manager->createQuery('select count(a) from App\Entity\Ad a')->getSingleScalarResult();

}
public function getBookingsCount(){
  return $this->manager->createQuery('select count(b) from App\Entity\Booking b')->getSingleScalarResult();

}
public function getCommentsCount(){
  return $this->manager->createQuery('select count(c) from App\Entity\Comment c')->getSingleScalarResult();

}

public function getAdsStats($direction){
return   $this->manager->createQuery(
    'select AVG(c.rating) as note,a.title, a.id, u.firstName, u.lastName, u.picture
    from App\Entity\Comment c
    join c.ad a
    join a.author u
    group by a
    order by note '.$direction
    )
    ->setMaxResults(5)
    ->getResult();
}

}
