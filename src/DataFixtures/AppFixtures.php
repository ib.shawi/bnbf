<?php

namespace App\DataFixtures;

use App\Entity\Ad;
use App\Entity\Role;
use App\Entity\Image;
use App\Entity\User;
use App\Entity\Booking;
use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
  private $encoder;
public function __construct(UserPasswordEncoderInterface $en){
  $this->encoder=$en;
}

    public function load(ObjectManager $manager)
    {

      $adminRole=new Role();
$adminRole->setTitle('ROLE_ADMIN');
$manager->persist($adminRole);
$adminUser=new User();
$adminUser->setFirstName("lior")
->setLastName("chamla")
->setEmail("lior@symfony.com")
->setIntroduction("information about author n sp")
->setDescription("information about author information about author information about author")
->setHash($this->encoder->encodePassword($adminUser,'password'))
->setPicture("img/a" . mt_rand(1,3) . ".jpg")
->setSlug("name names")
->addUserRole($adminRole);
$manager->persist($adminUser);

      $users=[];
        for($i=1;$i<=5;$i++){
$user =new User();
  $user->setFirstName("name$i")
  ->setLastName("names$i")
  ->setEmail("mail$i.com")
  ->setIntroduction("information about author n$i")
  ->setDescription("information about author $i information about author information about author")
  ->setHash($this->encoder->encodePassword($user,'password'))
  ->setPicture("img/a" . mt_rand(1,3) . ".jpg")
  ->setSlug("name$i names$i");
  $manager->persist($user);
  $users[$i]=$user;
        }

      for($i=1;$i<=30;$i++){
      $ad=new Ad();
      $ad->setTitle("titre n$i")
      ->setSlug("de-l-annonce-n-$i")
      ->setCoverImage("img/test.jpg")
      ->setIntroduction("bonjour a tous")
      ->setContent("rich content ddfklfk lskdldk sldklkkk")
      ->setPrice(mt_rand(40,200))
      ->setRooms(mt_rand(1,5))
      ->setAuthor($users[mt_rand(1,count($users)-1)]);
      ;

      for($j=1;$j<=mt_rand(2,3);$j++){
          $image=new Image;
          $image->setUrl("img/$j.jpg")
          ->setCaption("image$j")
          ->setAd($ad)
          ;
           $manager->persist($image);
        }

        for($j=1;$j<=mt_rand(0,5);$j++){
  $booking=new Booking();
  $createdAt=date_create("2013-03-15");
  $startDate=date_create("2013-06-15");
  $duration=mt_rand(3,6);
  $endDate=(clone $startDate)->modify("+$duration days");
  $amount=$ad->getPrice() * $duration;
  $booker=$users[mt_rand(1,count($users)-1)];
  $booking->setBooker($booker)
          ->setAd($ad)
          ->setStartDate($startDate)
          ->setEndDate($endDate)
          ->setCreatedAt($createdAt)
          ->setAmount($amount)
          ->setComment("asad sadd wedfdvf rtgrgg");
          $manager->persist($booking);

          if(mt_rand(0,1)){
          $comment=new Comment();
          $comment->setContent("asdd asssd ffdddf rrtttg")
          ->setRating(mt_rand(1,5))
          ->setAuthor($booker)
          ->setAd($ad)
          ;
          $manager->persist($comment);
          }
          }

      $manager->persist($ad);
      }
        $manager->flush();
    }
}
