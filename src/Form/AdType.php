<?php

namespace App\Form;

use App\Entity\Ad;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use App\Form\ImageType;

class AdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',TextType::class,['label'=>'title','attr'=>['placeholder'=>"enter title"]])
            ->add('slug',TextType::class,['label'=>'slug','attr'=>['placeholder'=>"automatique"],'required'=>false])
            ->add('price',MoneyType::class,['label'=>'price','attr'=>['placeholder'=>"enter price"]])
            ->add('introduction',TextType::class,['label'=>'intro','attr'=>['placeholder'=>"enter intro"]])
            ->add('content',TextareaType::class,['label'=>'content','attr'=>['placeholder'=>"enter lot of content"]])
            ->add('coverImage',TextType::class,['label'=>'image','attr'=>['placeholder'=>"enter image url"]])
            ->add('rooms',IntegerType::class,['label'=>'rooms','attr'=>['placeholder'=>"enter num of rooms"]])
            ->add('images',CollectionType::class,['entry_type'=>ImageType::class,
            'allow_add'=>true, 'allow_delete'=>true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
