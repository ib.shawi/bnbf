<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Comment;
use App\Form\AdminCommentType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Service\PaginationService;
use App\Repository\CommentRepository;

class AdminCommentController extends AbstractController
{

  /**
   * @Route("/admin/comments/{page<\d+>?1}", name="admin_comment_index")
   */
  public function index($page,PaginationService $pagination)
  {

      $pagination->setEntityClass(Comment::class)
      ->setRoute('admin_comment_index')
                  ->setPage($page);

      return $this->render('admin/comment/index.html.twig', [
            'pagination' => $pagination
      ]);
  }

    /**
     * @Route("/admin/comments/{id}/edit", name="admin_comment_edit")
     */
    public function edit(Comment $comment,Request $request, ObjectManager $manager)
    {

      $form=$this->createForm(AdminCommentType::class,$comment);
      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()){
      $manager->persist($comment);
      $manager->flush();
    $this->addFlash('success','modification happened');}

        return $this->render('admin/comment/edit.html.twig', [
            'comment' => $comment,'form'=>$form->createView()
        ]);
    }


    /**
     * @Route("/admin/comments/{id}/delete", name="admin_comment_delete")
     */
    public function delete(Comment $comment, ObjectManager $manager)
    {

      $manager->remove($comment);
      $manager->flush();
    $this->addFlash('success','deleted');
return $this->redirectToRoute('admin_comment_index');
    }
}
