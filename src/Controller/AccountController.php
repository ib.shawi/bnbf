<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Entity\PasswordUpdate;
use App\Form\PasswordUpdateType;
use App\Form\AccountType;
use Symfony\Component\Form\FormError;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AccountController extends AbstractController
{
    /**
     * @Route("/login", name="account_login")
     */
    public function login(AuthenticationUtils $utils)
    {
      $error=$utils->getLastAuthenticationError();
      $username=$utils->getLastUsername();
        return $this->render('account/login.html.twig',['hassError'=>$error !== null,'username'=>$username]);
    }

    /**
     * @Route("/logout", name="account_logout")
     */
    public function logout()
    {
          }

          /**
           * @Route("/register", name="account_register")
           */
          public function register(Request $request,ObjectManager $manager,UserPasswordEncoderInterface $encoder)
          {
            $user=new User();
$form=$this->createForm(RegistrationType::class,$user);
$form->handleRequest($request);
if($form->isSubmitted() && $form->isValid()){
$hash= $encoder->encodePassword($user,$user->getHash());
$user->setHash($hash);
$user->setSlug('test');
$manager->persist($user);
$manager->flush();
$this->addFlash('success','your account has been created you can login in now');
return $this->redirectToRoute('account_login');

}
return $this->render('account/form.html.twig',['form'=>$form->createView(),'buttonB'=>'submit','titleB'=>'register']);
                }

                /**
               * @Route("account/profile", name="account_profile")
               * @IsGranted("ROLE_USER")
               */
              public function profile(Request $request,ObjectManager $manager)
              {
                $user=$this->getUser();
      $form=$this->createForm(AccountType::class,$user);
       $form->handleRequest($request);
       if($form->isSubmitted() && $form->isValid()){

      $manager->persist($user);
      $manager->flush();
     $this->addFlash('success','your modification is saved');

     }
      return $this->render('account/form.html.twig',['form'=>$form->createView(),'buttonB'=>'edit','titleB'=>'profile']);
                    }

                    /**
               * @Route("account/password-update", name="account_password")
               * @IsGranted("ROLE_USER")
               */
              public function updatePassword(Request $request,ObjectManager $manager,UserPasswordEncoderInterface $encoder)
              {
                $user=$this->getUser();
                $passwordUpdate=new PasswordUpdate();
      $form=$this->createForm(PasswordUpdateType::class,$passwordUpdate);
       $form->handleRequest($request);
       if($form->isSubmitted() && $form->isValid()){
     if(!password_verify($passwordUpdate->getOldPassword(),$user->getHash())){

        $form->get('oldPassword')->addError(new FormError("old password does not match"));

     }
     else{
       $newPassword=$passwordUpdate->getNewPassword();
       $hash=$encoder->encodePassword($user,$newPassword);
       $user->setHash($hash);
      $manager->persist($user);
      $manager->flush();
     $this->addFlash('success','your modification is saved');
     return $this->redirectToRoute('home');
}
     }
      return $this->render('account/form.html.twig',['form'=>$form->createView(),'buttonB'=>'confirm','titleB'=>'password']);
                    }

        /**
         * @Route("/account", name="account_index")
         * @IsGranted("ROLE_USER")
         */
        public function myAccount()
        {
            return $this->render('user/index.html.twig',['user'=>$this->getUser(),'tmp'=>true]);
        }
        /**
         * @Route("/account/bookings", name="account_bookings")
         *@IsGranted("ROLE_USER")
         */
        public function bookings()
        {
            return $this->render('account/bookings.html.twig');
        }
}
