<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AdRepository;
use App\Entity\Ad;
use App\Form\AdType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Service\PaginationService;

class AdminAdController extends AbstractController
{

  /**
   * @Route("/admin/ads/{page<\d+>?1}", name="admin_ads_index")
   */
  public function index(AdRepository $repo,$page,PaginationService $pagination)
  {
    $pagination->setEntityClass(Ad::class)
    ->setRoute('admin_ads_index')
                ->setPage($page);

      return $this->render('admin/ad/index.html.twig', [
          'pagination' => $pagination
      ]);
  }

    /**
     * @Route("/admin/ads/{id}/edit", name="admin_ads_edit")
     */
    public function edit(Ad $ad,Request $request,ObjectManager $manager)
    {
      $form=$this->createForm(AdType::class,$ad);
      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()){
        $manager->persist($ad);
        $manager->flush();
        $this->addFlash('success','modification is done');

      }
        return $this->render('admin/ad/edit.html.twig', [
            'ad' => $ad,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/admin/ads/{id}/delete", name="admin_ads_delete")
     */
    public function delete(Ad $ad,ObjectManager $manager)
    {
      if(count($ad->getBookings())>0){
        $this->addFlash('danger','there is reservations');
      }
      else{
      $manager->remove($ad);
      $manager->flush();
      $this->addFlash('success','ad removed');}
      return $this->redirectToRoute('admin_ads_index');

    }



}
