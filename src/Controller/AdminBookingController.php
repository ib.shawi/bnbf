<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BookingRepository;
use App\Entity\Booking;
use App\Form\AdminBookingType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Service\PaginationService;

class AdminBookingController extends AbstractController
{
  /**
   * @Route("/admin/bookings/{page<\d+>?1}", name="admin_booking_index")
   */
  public function index(BookingRepository $repo,$page,PaginationService $pagination)
  {
    $pagination->setEntityClass(Booking::class)
    ->setRoute('admin_booking_index')
                ->setPage($page);

      return $this->render('admin/booking/index.html.twig', [
          'pagination' => $pagination
      ]);
  }

    /**
     * @Route("/admin/bookings/{id}/edit", name="admin_booking_edit")
     */
    public function edit(Booking $book,Request $request,ObjectManager $manager)
    {

      $form=$this->createForm(AdminBookingType::class,$book);
      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()){
        $book->setAmount(0);
      $manager->persist($book);
      $manager->flush();
    $this->addFlash('success','modification happened');
return $this->redirectToRoute('admin_booking_index');
  }

        return $this->render('admin/booking/edit.html.twig', [
            'booking' => $book,
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/admin/bookings/{id}/delete", name="admin_booking_delete")
     */
    public function delete(Booking $booking, ObjectManager $manager)
    {

      $manager->remove($booking);
      $manager->flush();
    $this->addFlash('success','deleted');
return $this->redirectToRoute('admin_booking_index');
    }
}
