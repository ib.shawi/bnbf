<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use App\Entity\Ad;
use App\Repository\AdRepository;
use App\Form\AdType;
use App\Entity\Image;

class AdController extends Controller
{

/**
* @Route("/ads",name="ads_index")
*/
public function index(AdRepository $repo)
{
  //$repo=$this->getDoctrine()->getRepository(Ad::class);
  $ads=$repo->findAll();

return $this->render('ad/index.html.twig',['ads'=>$ads]);
}

/**
* @Route("/ads/new",name="ads_create")
* @IsGranted("ROLE_USER")
*/
public function create(Request $request, ObjectManager $manager)
{
  $ad=new Ad();

  $form=$this->createForm(AdType::class,$ad);
  $form->handleRequest($request);
  if($form->isSubmitted() && $form->isValid()){
    foreach($ad->getImages() as $image){
  $image->setAd($ad);
  $manager->persist($image);
}
$ad->setAuthor($this->getUser());
    $manager->persist($ad);
    $manager->flush();

    $this->addFlash('success','record is created');
    return $this->redirectToRoute('ads_show',['id'=>$ad->getId()]);
  }
return $this->render('ad/new.html.twig',[
  'form'=>$form->createView(),'actionB'=>'create record'
]);
}

/**
 * @Route("/ads/{id}/edit", name="ads_edit")
 * @Security("is_granted('ROLE_USER') and user === ad.getAuthor()")
 */
public function edit(Ad $ad,Request $request,ObjectManager $manager)
{
  $form= $this->createForm(AdType::class,$ad);
  $form->handleRequest($request);
  if($form->isSubmitted() && $form->isValid()){
    foreach($ad->getImages() as $image){
      $image->setAd($ad);
      $manager->persist($image);
    }
    $manager->persist($ad);
    $manager->flush();
    $this->addFlash('success','votre item est modifie');
    return $this->redirectToRoute('ads_show',['id' => $ad->getId()]);
  }
   return $this->render('ad/new.html.twig',['form'=>$form->createView(),'ad'=>$ad,'actionB'=>'edit record']);
}

/**
* @Route("/ads/{id}",name="ads_show")
*/
public function show($id, AdRepository $repo)
{
  $ad=$repo->findOneById($id);

return $this->render('ad/show.html.twig',['ad'=>$ad]);
}

/**
 * @Route("/ads/{id}/delete", name="ads_delete")
 *@Security("is_granted('ROLE_USER') and user === ad.getAuthor()")
 */
public function delete(Ad $ad,ObjectManager $manager)
{

    $manager->remove($ad);
    $manager->flush();
    $this->addFlash('success','votre item est suprimee');
    return $this->redirectToRoute('ads_index');

}
}
